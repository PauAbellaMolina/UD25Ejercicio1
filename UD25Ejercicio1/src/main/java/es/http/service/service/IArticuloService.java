package es.http.service.service;

import java.util.List;

import es.http.service.dto.Articulo;

public interface IArticuloService {

	public List<Articulo> listarArticulos();
	
	public Articulo guardarArticulo(Articulo articulo);

	public Articulo encontrarArticuloId(Long id);
	
	public Articulo actualizarArticulo(Articulo articulo);
	
	public void eliminarArticulo(Long id);
}
