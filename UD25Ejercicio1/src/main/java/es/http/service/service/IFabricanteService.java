package es.http.service.service;

import java.util.List;

import es.http.service.dto.Fabricante;

public interface IFabricanteService {

public List<Fabricante> listarFabricantes();
	
	public Fabricante guardarFabricante(Fabricante empleado);

	public Fabricante encontrarFabricanteId(Long id);
	
	public Fabricante actualizarFabricante(Fabricante empleado);
	
	public void eliminarFabricante(Long id);
}
