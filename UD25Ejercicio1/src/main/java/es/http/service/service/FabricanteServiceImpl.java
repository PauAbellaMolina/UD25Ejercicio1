package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IFabricanteDAO;
import es.http.service.dto.Fabricante;

@Service
public class FabricanteServiceImpl {
	
	@Autowired
	IFabricanteDAO iFabricanteDAO;
	
	public List<Fabricante> listarFabricante() {
		return iFabricanteDAO.findAll();
	}
	
	public Fabricante guardarFabricante(Fabricante fabricante) {
		return iFabricanteDAO.save(fabricante);
	}
	
	public Fabricante encontrarFabricanteId(Long id) {
		return iFabricanteDAO.findById(id).get();
	}
	
	public Fabricante actualizarFabricante(Fabricante fabricante) {
		return iFabricanteDAO.save(fabricante);
	}
	
	public void eliminarFabricante(Long id) {
		iFabricanteDAO.deleteById(id);
	}
}
