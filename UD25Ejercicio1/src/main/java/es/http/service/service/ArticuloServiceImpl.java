package es.http.service.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.http.service.dao.IArticuloDAO;
import es.http.service.dto.Articulo;

@Service
public class ArticuloServiceImpl {
	
	@Autowired
	IArticuloDAO iArticuloDAO;
	
	public List<Articulo> listarArticulos() {
		return iArticuloDAO.findAll();
	}
	
	public Articulo guardarArticulo(Articulo articulo) {
		return iArticuloDAO.save(articulo);
	}
	
	public Articulo encontrarArticuloId(Long id) {
		return iArticuloDAO.findById(id).get();
	}
	
	public Articulo actualizarArticulo(Articulo articulo) {
		return iArticuloDAO.save(articulo);
	}
	
	public void eliminarArticulo(Long id) {
		iArticuloDAO.deleteById(id);
	}
}
