package es.http.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import es.http.service.dto.Articulo;

public interface IArticuloDAO extends JpaRepository<Articulo, Long> {

}
