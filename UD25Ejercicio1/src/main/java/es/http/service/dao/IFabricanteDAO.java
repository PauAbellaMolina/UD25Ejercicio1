package es.http.service.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import es.http.service.dto.Fabricante;

public interface IFabricanteDAO extends JpaRepository<Fabricante, Long> {

}
