package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Articulo;
import es.http.service.service.ArticuloServiceImpl;

@RestController
@RequestMapping("/api")
public class ArticuloController {
	
	@Autowired
	ArticuloServiceImpl articuloServiceImpl;
	
	@GetMapping("/articulos")
	public List<Articulo> listarArticulos() {
		return articuloServiceImpl.listarArticulos();
	}
	
	@PostMapping("/articulos")
	public Articulo guardarArticuloController(@RequestBody Articulo articulo){
		return articuloServiceImpl.guardarArticulo(articulo);
	}
	
	@GetMapping("/articulos/{id}")
	public Articulo encontrarArticuloIdController(@PathVariable(name="id") Long id){
		Articulo articuloEncontrado = new Articulo();
		articuloEncontrado = articuloServiceImpl.encontrarArticuloId(id);
		return articuloEncontrado;
	}
	
	@PutMapping("/articulos/{id}")
	public Articulo actualizarArticuloController(@PathVariable(name="id") Long id, @RequestBody Articulo articulo){
		Articulo articuloSeleccionado = new Articulo();
		Articulo articuloActualizado = new Articulo();
		
		articuloSeleccionado = articuloServiceImpl.encontrarArticuloId(id);
		
		articuloSeleccionado.setNombre(articulo.getNombre());
		articuloSeleccionado.setPrecio(articulo.getPrecio());
		articuloSeleccionado.setFabricante(articulo.getFabricante());
		
		articuloActualizado = articuloServiceImpl.actualizarArticulo(articuloSeleccionado);
		
		return articuloActualizado;
	}
	
	@DeleteMapping("/articulos/{id}")
	public void eliminarArticuloController(@PathVariable(name="id") Long id){
		articuloServiceImpl.eliminarArticulo(id);
	}
}
