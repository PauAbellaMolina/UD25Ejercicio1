package es.http.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import es.http.service.dto.Fabricante;
import es.http.service.service.FabricanteServiceImpl;

@RestController
@RequestMapping("/api")
public class FabricanteController {
	
	@Autowired
	FabricanteServiceImpl fabricanteServiceImpl;
	
	@GetMapping("/fabricantes")
	public List<Fabricante> listarFabricantes() {
		return fabricanteServiceImpl.listarFabricante();
	}
	
	@PostMapping("/fabricantes")
	public Fabricante guardarFabricanteController(@RequestBody Fabricante fabricante){
		return fabricanteServiceImpl.guardarFabricante(fabricante);
	}
	
	@GetMapping("/fabricantes/{id}")
	public Fabricante encontrarFabricanteIdController(@PathVariable(name="id") Long id){
		Fabricante fabricanteEncontrado = new Fabricante();
		fabricanteEncontrado = fabricanteServiceImpl.encontrarFabricanteId(id);
		return fabricanteEncontrado;
	}
	
	@PutMapping("/fabricantes/{id}")
	public Fabricante actualizarFabricanteController(@PathVariable(name="id") Long id, @RequestBody Fabricante fabricante){
		Fabricante fabricanteSeleccionado = new Fabricante();
		Fabricante fabricanteActualizado = new Fabricante();
		
		fabricanteSeleccionado = fabricanteServiceImpl.encontrarFabricanteId(id);
		
		fabricanteSeleccionado.setNombre(fabricante.getNombre());
		
		fabricanteActualizado = fabricanteServiceImpl.actualizarFabricante(fabricanteSeleccionado);
		
		return fabricanteActualizado;
	}
	
	@DeleteMapping("/fabricantes/{id}")
	public void eliminarFabricanteController(@PathVariable(name="id") Long id){
		fabricanteServiceImpl.eliminarFabricante(id);
	}
}
