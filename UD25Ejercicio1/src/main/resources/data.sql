DROP table IF EXISTS articulos;
DROP table IF EXISTS fabricantes;

CREATE TABLE `fabricantes` (
  `id` long NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `articulos` (
  `id` long NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `precio` int DEFAULT NULL,
  `fab_id` long DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fabricantes_fk` FOREIGN KEY (`fab_id`) REFERENCES `fabricantes` (`id`)
);

insert into fabricantes (nombre)values('Asus');
insert into fabricantes (nombre)values('Acer');
insert into fabricantes (nombre)values('Dell');

insert into articulos (nombre,precio,fab_id)values('RTX 2080',899,1);
insert into articulos (nombre,precio,fab_id)values('Impresora 3000',299,2);
insert into articulos (nombre,precio,fab_id)values('Monitor HD',649,3);
